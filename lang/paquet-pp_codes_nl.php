<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Beheer van een knoppenbalk met informatica codes door middel van de knoppenbalk van Porte Plume, de Penhouder.
Voegt ook een snelkoppeling <code>[->ecrire/inc_versions.php#trac]</code> toe.', # MODIF
	'pp_codes_nom' => 'Informatica-codes voor de Penhouder',
	'pp_codes_slogan' => 'Voegt knoppen toe aan Porte Plume, de Penhouder, voor informatica-codes'
);
