<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Gestión de botones para insertar el código informático a través de la barra de edición del Portaplumas.
Añade también un acceso directo <code>[->ecrire/inc_versions.php#trac]</code>.',
	'pp_codes_nom' => 'Códigos Informáticos para Portaplumas',
	'pp_codes_slogan' => 'Añade botones a la barra de herramientas del Portaplumas para generar códigos informáticos'
);
