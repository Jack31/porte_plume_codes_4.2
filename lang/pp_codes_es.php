<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pp_codes?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_activer_barre_edition' => 'Barra de edición',
	'label_activer_barre_forum' => 'Barra de foro',
	'label_outils_actifs' => 'Herramientas activas',

	// O
	'outil_inserer_cadre_biblatex' => 'Insertar código BibLaTeX preformateado (marco)',
	'outil_inserer_cadre_bibtex' => 'Insertar código BibTeX preformateado (marco)',
	'outil_inserer_cadre_css' => 'Insertar un código preformateado CSS (marco)',
	'outil_inserer_cadre_html' => 'Insertar un código preformateado HTML (cuadro)',
	'outil_inserer_cadre_latex' => 'Insertar código LaTeX preformateado (marco)',
	'outil_inserer_cadre_php' => 'Insertar un código preformateado PHP (marco)',
	'outil_inserer_cadre_spip' => 'Insertar un código preformateado SPIP (marco)',
	'outil_inserer_cadre_xml' => 'Insertar un código preformateado XML (marco)',
	'outil_inserer_kbd' => 'Insertar una entrada teclado (kdb)',
	'outil_inserer_lien_trac' => 'Insertar un enlace al trac de SPIP',
	'outil_inserer_pre' => 'Insertar un código preformateado (pre)',
	'outil_inserer_samp' => 'Insertar una salida de código (samp)',
	'outil_inserer_var' => 'Insertar una variable (var)',

	// P
	'pp_codes' => 'Códigos informáticos para Portaplumas',

	// T
	'titre_activer_extension_sur' => '¿Activar en qué barras de herramientas?',
	'titre_configurer_pp_codes' => 'Configurar la extensión códigos informáticos para Portaplumas'
);
